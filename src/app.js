//inc file system module
fs = require("fs")

//read data
const data = fs.readFileSync("data/cities.json", "utf-8")

//convert json
const cities = JSON.parse(data)

//display
for(const city of cities){
    console.log(city.name)
}